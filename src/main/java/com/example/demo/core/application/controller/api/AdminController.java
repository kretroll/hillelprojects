package com.example.demo.core.application.controller.api;

import com.example.demo.core.application.dto.ChangeRoleRequestDto;
import com.example.demo.core.application.dto.PageDto;
import com.example.demo.core.application.dto.UserDto;
import com.example.demo.core.application.facade.AdminFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Admin Controller")
@RestController
@RequestMapping("/admin")
public class AdminController {
    private final AdminFacade adminFacade;

    public AdminController(AdminFacade adminFacade) {
        this.adminFacade = adminFacade;
    }

    @ApiOperation(value = "GetAllUsers", notes = "Listing all users from database with pagination params")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public PageDto<UserDto> getAll(@RequestParam(required = false, defaultValue = "0") Integer startIndex,
                                   @RequestParam(required = false, defaultValue = "0") Integer size) {
        return adminFacade.getAllUsers(startIndex, size);
    }

    @ApiOperation(value = "GetUser", notes = "Getting user by Id from database")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUser(@PathVariable("id") Long userId) throws NotFoundException {
        return adminFacade.getUser(userId);
    }

    @ApiOperation(value = "AddUser", notes = "Adding user to database")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addUser(@RequestBody @Valid UserDto userDto) {
        adminFacade.addUser(userDto);
    }

    @ApiOperation(value = "DeleteUserById", notes = "Deleting user by Id from database")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long userId){
        adminFacade.deleteById(userId);
    }

    @ApiOperation(value = "DeleteAllUsers", notes = "Deleting all users from database")
    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllUsers(){
        adminFacade.deleteAllUsers();
    }

    @ApiOperation(value = "ReplaceUser", notes = "Replacing user by Id with new params with existing user from database")
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void replaceUser(@RequestBody @Valid UserDto newUser, @PathVariable Long id) throws NotFoundException {
        adminFacade.replaceUser(newUser, id);
    }

    @ApiOperation(value = "GroupsUsersByAge", notes = "Grouping users from DB by age")
    @GetMapping("/groupUsersByAge")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> groupUsersByAge() {
        return adminFacade.groupUsersByAge();
    }

    @ApiOperation(value = "GetAllUsersFromClient", notes = "Getting all users from DB without pagination")
    @GetMapping("/getAll")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsersFromClient() {
        return adminFacade.getAllUsersFromClient();
    }

    @ApiOperation(value = "ChangeRole", notes = "Changing role from USER to ADMIN and otherwise")
    @PatchMapping("/changeRole")
    @ResponseStatus(HttpStatus.OK)
    public void changeRole(@RequestBody ChangeRoleRequestDto changeRoleRequestDto){
        adminFacade.changeRole(changeRoleRequestDto);
    }
}
