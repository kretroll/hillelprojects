package com.example.demo.core.application.facade;

import com.example.demo.core.application.dto.ChangeRoleRequestDto;
import com.example.demo.core.application.dto.PageDto;
import com.example.demo.core.application.dto.UserDto;
import com.example.demo.core.database.entity.Role;
import com.example.demo.core.domain.model.User;
import com.example.demo.core.domain.service.UserService;
import com.example.demo.core.mapper.UserMapper;
import javassist.NotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AdminFacade {
    private final UserService userService;
    private final UserMapper userMapper;

    public AdminFacade(UserService userService, UserMapper userMapper){
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public PageDto<UserDto> getAllUsers(Integer startIndex, Integer size) {
        return userService.getAllUsers(startIndex, size);
    }

    public UserDto getUser(Long id) throws NotFoundException {
        User user = userService.getUser(id);
        return userMapper.toDto(user);
    }

    public void addUser(UserDto userDto)  {
        userService.addUser(userDto);
    }

    public void deleteById(Long id) {
        userService.deleteById(id);
    }

    public void deleteAllUsers() {
        userService.deleteAllUsers();
    }

    public void replaceUser(UserDto userDto,Long id) throws NotFoundException{
        userService.replaceUser(userDto, id);

    }

    public List<UserDto> groupUsersByAge(){
        return userService.groupUsersByAge().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<UserDto> getAllUsersFromClient(){
        return userService.getAllUsersFromClient().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());

    }

    public void changeRole(ChangeRoleRequestDto changeRoleRequestDto){
        userService.changeRole(changeRoleRequestDto.getId(), changeRoleRequestDto.getRole());
    }
}
