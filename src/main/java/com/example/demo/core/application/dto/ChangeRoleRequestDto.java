package com.example.demo.core.application.dto;

import com.example.demo.core.database.entity.Role;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "Change Role Request Dto", description = "Dto model for changing role from USER to ADMIN and otherwise")
public class ChangeRoleRequestDto {
    @NotBlank
    Long id;

    @NotNull
    Role role;
}
