package com.example.demo.core.application.controller.api;

import com.example.demo.core.application.dto.BankAccountDto;
import com.example.demo.core.application.facade.BankAccountFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "BankAccount Controller")
@RestController
@RequestMapping("/api/bankAccounts")
public class BankAccountController {
    private final BankAccountFacade bankAccountFacade;

    public BankAccountController(BankAccountFacade bankAccountFacade) {
        this.bankAccountFacade = bankAccountFacade;
    }

    @ApiOperation(value = "GetAllBankAccounts", notes = "Listing all bank accounts from database")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<BankAccountDto> getAllBankAccounts(){
        return bankAccountFacade.getAll();
    }

    @ApiOperation(value = "GetBankAccount", notes = "Getting bank account from DB by Id")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BankAccountDto getBankAccount(@PathVariable("id") Long bankAccountId) throws NotFoundException {
        return bankAccountFacade.getBankAccount(bankAccountId);
    }
}