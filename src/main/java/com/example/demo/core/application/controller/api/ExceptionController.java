package com.example.demo.core.application.controller.api;

import com.example.demo.core.application.dto.ApiError;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@Api(value = "Exception Controller")
@ControllerAdvice
public class ExceptionController {
    @ApiOperation(value = "ExceptionHandler", notes = "Handling exceptions from Api Error class")
    @ExceptionHandler
    public ResponseEntity<ApiError> handle(MethodArgumentNotValidException exception) {
        return new ResponseEntity<>(new ApiError(LocalDateTime.now(), exception.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
