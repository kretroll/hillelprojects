package com.example.demo.core.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel(value = "Add BankAccount Request", description = "Request Model for creating bankAccount")
@Getter
@Setter
public class BankAccountDto {
    @ApiModelProperty(value = "Id of bankAccount", example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(value = "Name of bankAccount", example = "bankAccount", required = true)
    @NotBlank
    private String name;

    @ApiModelProperty(value = "Amount of many on bank account", example = "1000000", required = true)
    private BigDecimal bigDecimal;

}
