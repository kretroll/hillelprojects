package com.example.demo.core.application.facade;

import com.example.demo.core.application.dto.BankAccountDto;
import com.example.demo.core.domain.model.BankAccount;
import com.example.demo.core.domain.service.BankAccountService;
import com.example.demo.core.mapper.BankAccountMapper;
import javassist.NotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BankAccountFacade {
    private final BankAccountService bankAccountService;
    private final BankAccountMapper bankAccountMapper;

    public BankAccountFacade(BankAccountService bankAccountService, BankAccountMapper userMapper){
        this.bankAccountService = bankAccountService;
        this.bankAccountMapper = userMapper;
    }

    public List<BankAccountDto> getAll(){
        return bankAccountService.getAllBankAccounts().stream()
                .map(bankAccountMapper::toDto)
                .collect(Collectors.toList());
    }

    public BankAccountDto getBankAccount(Long id) throws NotFoundException {
        BankAccount bankAccount = bankAccountService.getBankAccount(id);
        return bankAccountMapper.toDto(bankAccount);
    }
}
