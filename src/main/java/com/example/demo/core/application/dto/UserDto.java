package com.example.demo.core.application.dto;

import com.example.demo.core.application.validator.UserAgeConstraint;
import com.example.demo.core.database.entity.Gender;
import com.example.demo.core.database.entity.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.util.List;

@ApiModel(value = "Add user Request", description = "Request Model for creating user")
@Getter
@Setter
public class UserDto {
    @ApiModelProperty(value = "User gender", example = "MALE", required = true)
    @NotNull
    private Gender gender;

    @ApiModelProperty(value = "User's first name", example = "Marina", required = true)
    @NotBlank
    private String firstName;

    @ApiModelProperty(value = "User's last name'", example = "Kenedy", required = true)
    @NotBlank
    private String lastName;

    @ApiModelProperty(value = "Email/username of user'", example = "email@gmail.com", required = true)
    @NotBlank
    @Pattern(regexp = "^[a-z0-9](\\.?[a-z0-9]){5,}@g(oogle)?mail\\.com$", message = "Email not valid")
    private String email;

    @ApiModelProperty(value = "User's role", example = "USER", required = true)
    @NotNull
    private String role;

    @ApiModelProperty(value = "User's password", example = "Query11234", required = true)
    @NotBlank
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$", message = "Password not valid")
    private String password;

    @ApiModelProperty(value = "User's age, from 18 to 100 only", example = "20", required = true)
    @Min(18)
    @Max(100)
    @UserAgeConstraint
    private Integer age;

    private HospitalDto hospital;

    private List<BankAccountDto> bankAccounts;
}
