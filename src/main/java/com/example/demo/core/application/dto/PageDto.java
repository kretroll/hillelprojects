package com.example.demo.core.application.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@ApiModel(value = "Page parameters", description = "Parameters of page for Pagination")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PageDto<T> {
    private int pagesNumber;
    private Long elementsNumber;
    private List<T> content;
}