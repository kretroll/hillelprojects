package com.example.demo.core.application.controller.api;

import com.example.demo.core.application.dto.HospitalDto;
import com.example.demo.core.application.facade.HospitalFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "Hospital Controller")
@RestController
@RequestMapping("/api/hospitals")
public class HospitalController {
    private final HospitalFacade hospitalFacade;

    public HospitalController(HospitalFacade hospitalFacade) {
        this.hospitalFacade = hospitalFacade;
    }

    @ApiOperation(value = "GetAllHospitals", notes = "Getting all hospitals from DB")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<HospitalDto> getAllHospitals(){
        return hospitalFacade.getAll();
    }

    @ApiOperation(value = "GetHospital", notes = "Getting hospital by Id from database")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public HospitalDto getHospital(@PathVariable("id") Long hospitalId) throws NotFoundException {
        return hospitalFacade.getHospital(hospitalId);
    }

    @ApiOperation(value = "AddHospital", notes = "Adding hospital to database")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addHospital(@RequestBody HospitalDto hospitalDto) {
        hospitalFacade.addHospital(hospitalDto);
    }
}
