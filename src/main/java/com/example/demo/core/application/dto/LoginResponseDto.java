package com.example.demo.core.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "Login response Model", description = "Request Model that creating token for user")
@Getter
@Setter
@AllArgsConstructor
public class LoginResponseDto {
    @ApiModelProperty(value = "User token", example = "12faishf1u2489124y1uifh9hfafafsasfgaa", required = true)
    @NotBlank
    public String token;
}
