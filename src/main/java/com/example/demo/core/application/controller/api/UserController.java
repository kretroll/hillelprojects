package com.example.demo.core.application.controller.api;

import com.example.demo.core.application.dto.PageDto;
import com.example.demo.core.application.dto.UserDto;
import com.example.demo.core.application.facade.UserFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "User Controller")
@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserFacade userFacade;

    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @ApiOperation(value = "GetAllUsers", notes = "Listing all users from database with pagination params")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public PageDto<UserDto> getAll(@RequestParam(required = false, defaultValue = "0") Integer startIndex,
                                   @RequestParam(required = false, defaultValue = "0") Integer size) {
        return userFacade.getAllUsers(startIndex, size);
    }

    @ApiOperation(value = "GetUser", notes = "Getting user by Id from database")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUser(@PathVariable("id") Long userId) throws NotFoundException {
        return userFacade.getUser(userId);
    }

    @ApiOperation(value = "GroupsUsersByAge", notes = "Grouping users from DB by age")
    @GetMapping("/groupUsersByAge")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> groupUsersByAge() {
        return userFacade.groupUsersByAge();
    }

    @ApiOperation(value = "GetAllUsersFromClient", notes = "Getting all users from DB without pagination")
    @GetMapping("/getAll")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsersFromClient() {
        return userFacade.getAllUsersFromClient();
    }
}
