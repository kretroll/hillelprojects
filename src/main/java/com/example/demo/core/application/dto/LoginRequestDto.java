package com.example.demo.core.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@ApiModel(value = "Login user Model", description = "Request Model for user to login")
@Getter
@Setter
public class LoginRequestDto {
    @ApiModelProperty(value = "User's username", example = "email@gmail.com", required = true)
    @Email
    public String username;

    @ApiModelProperty(value = "User's password", example = "Query1234$", required = true)
    @NotBlank
    public String password;
}
