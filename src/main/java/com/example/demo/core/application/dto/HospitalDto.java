package com.example.demo.core.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(value = "Add Hospital Request", description = "Request Model for adding hospital")
@Getter
@Setter
public class HospitalDto {
    @ApiModelProperty(value = "Hospital's id", example = "12", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(value = "Hospital's name", example = "Hospital1", required = true)
    @NotBlank
    private String hospital_name;
}
