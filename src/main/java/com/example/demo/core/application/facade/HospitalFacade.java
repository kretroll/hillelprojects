package com.example.demo.core.application.facade;

import com.example.demo.core.application.dto.HospitalDto;
import com.example.demo.core.application.dto.UserDto;
import com.example.demo.core.domain.model.Hospital;
import com.example.demo.core.domain.service.HospitalService;
import com.example.demo.core.mapper.HospitalMapper;
import javassist.NotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class HospitalFacade {
    private final HospitalService hospitalService;
    private final HospitalMapper hospitalMapper;

    public HospitalFacade(HospitalService hospitalService, HospitalMapper hospitalMapper){
        this.hospitalService = hospitalService;
        this.hospitalMapper = hospitalMapper;
    }

    public List<HospitalDto> getAll(){
        return hospitalService.getAllHospitals().stream()
                .map(hospitalMapper::toDto)
                .collect(Collectors.toList());
    }

    public HospitalDto getHospital(Long id) throws NotFoundException {
        Hospital hospital = hospitalService.getHospital(id);
        return hospitalMapper.toDto(hospital);
    }

    public void addHospital(HospitalDto hospitalDto)  {
        hospitalService.addHospital(hospitalDto);
    }
}