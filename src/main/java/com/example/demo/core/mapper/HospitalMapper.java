package com.example.demo.core.mapper;

import com.example.demo.core.application.dto.HospitalDto;
import com.example.demo.core.database.entity.HospitalEntity;
import com.example.demo.core.domain.model.Hospital;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class HospitalMapper {
    private final ModelMapper mapper;

    public HospitalMapper(ModelMapper mapper){
        this.mapper = mapper;
    }

    public HospitalDto toDto(Hospital hospital){
        return Objects.isNull(hospital) ? null: mapper.map(hospital, HospitalDto.class);
    }

    public Hospital toModel(HospitalEntity hospitalEntity) {
        return Objects.isNull(hospitalEntity) ? null: mapper.map(hospitalEntity, Hospital.class);
    }

    public HospitalEntity toEntity(HospitalDto hospitalDto) {
        return Objects.isNull(hospitalDto) ? null : mapper.map(hospitalDto, HospitalEntity.class);
    }
}
