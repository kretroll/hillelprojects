package com.example.demo.core.mapper;

import com.example.demo.core.application.dto.BankAccountDto;
import com.example.demo.core.database.entity.BankAccountEntity;
import com.example.demo.core.domain.model.BankAccount;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class BankAccountMapper {
    private final ModelMapper mapper;

    public BankAccountMapper(ModelMapper mapper){
        this.mapper = mapper;
    }

    public BankAccountDto toDto(BankAccount bankAccount){
        return Objects.isNull(bankAccount) ? null: mapper.map(bankAccount, BankAccountDto.class);
    }

    public BankAccount toModel(BankAccountEntity bankAccountEntity) {
        return Objects.isNull(bankAccountEntity) ? null: mapper.map(bankAccountEntity, BankAccount.class);
    }
}
