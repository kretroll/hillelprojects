package com.example.demo.core.mapper;

import com.example.demo.core.application.dto.UserDto;
import com.example.demo.core.database.entity.UserEntity;
import com.example.demo.core.domain.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;

    private final ModelMapper mapper;

    public UserMapper(ModelMapper mapper, PasswordEncoder passwordEncoder){
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto toDto(User user){
        return Objects.isNull(user) ? null: mapper.map(user, UserDto.class);
    }

    public User toModel(UserEntity userEntity) {
        return Objects.isNull(userEntity) ? null: mapper.map(userEntity, User.class);
    }

    public UserEntity toEntity(UserDto userDto) {
        UserEntity userEntity = mapper.map(userDto, UserEntity.class);
        userEntity.setPassword(passwordEncoder.encode(userDto.getPassword()));
        return userEntity;
    }

}
