package com.example.demo.core.database.repositories;

import com.example.demo.core.database.entity.BankAccountEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankAccountRepository extends PagingAndSortingRepository<BankAccountEntity, Long> {
}
