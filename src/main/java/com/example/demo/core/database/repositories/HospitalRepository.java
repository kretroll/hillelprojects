package com.example.demo.core.database.repositories;

import com.example.demo.core.database.entity.HospitalEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HospitalRepository extends PagingAndSortingRepository<HospitalEntity, Long> {

    Optional<HospitalEntity> findById(Long id);

}
