package com.example.demo.core.database.repositories;

import com.example.demo.core.database.entity.Role;
import com.example.demo.core.database.entity.UserEntity;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

    Optional<UserEntity> findByEmail(String email);
//
    @NonNull
    Page<UserEntity> findAll(@NonNull Pageable pageable);

    @Query(value = "SELECT * FROM demo_user GROUP BY id, age ORDER BY age", nativeQuery = true)
    List<UserEntity> groupByAge();

    @Modifying
    @Transactional
    @Query(value = "UPDATE demo_user SET role = ?1 WHERE id = ?2", nativeQuery = true)
    void changeRoleOfUser(String role, Long id);
}



