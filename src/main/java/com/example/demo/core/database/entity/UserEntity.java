package com.example.demo.core.database.entity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
@SequenceGenerator(name="seq", initialValue=1, allocationSize=100)
@Table(name = "demo_user")
public class UserEntity {
    @Id
    @Setter
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;

    @Setter
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    private Integer age;

    private String password;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "hospital", referencedColumnName = "id")
    private HospitalEntity hospital;

    @OneToMany(fetch=FetchType.EAGER, mappedBy = "client", cascade = CascadeType.ALL)
    private List<BankAccountEntity> bankAccounts;
}

