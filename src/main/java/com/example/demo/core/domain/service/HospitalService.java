package com.example.demo.core.domain.service;

import com.example.demo.core.application.dto.HospitalDto;
import com.example.demo.core.application.dto.UserDto;
import com.example.demo.core.database.repositories.HospitalRepository;
import com.example.demo.core.domain.model.Hospital;
import com.example.demo.core.mapper.HospitalMapper;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class HospitalService {

    private final HospitalRepository hospitalRepository;

    private final HospitalMapper hospitalMapper;

    public HospitalService(HospitalRepository hospitalRepository, HospitalMapper hospitalMapper) {
        this.hospitalRepository = hospitalRepository;
        this.hospitalMapper = hospitalMapper;
    }

    public List<Hospital> getAllHospitals(){
        return StreamSupport.stream(hospitalRepository.findAll().spliterator(), false)
                .map(hospitalMapper::toModel)
                .collect(Collectors.toList());
    }

    public Hospital getHospital(Long id) throws NotFoundException {
        Optional<Hospital> hospital = hospitalRepository.findById(id).map(hospitalMapper::toModel);
        return hospital.orElseThrow(() -> new NotFoundException(String.format("Hospital not found with id:%S", id)));
    }

    public void addHospital(HospitalDto hospitalDto) throws IllegalArgumentException {
        hospitalRepository.save(hospitalMapper.toEntity(hospitalDto));
    }
}
