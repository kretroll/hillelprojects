package com.example.demo.core.domain.service;

import com.example.demo.core.database.repositories.BankAccountRepository;
import com.example.demo.core.domain.model.BankAccount;
import com.example.demo.core.mapper.BankAccountMapper;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class BankAccountService {

    private final BankAccountRepository bankAccountRepository;

    private final BankAccountMapper bankAccountMapper;

    public BankAccountService(BankAccountRepository bankAccountRepository, BankAccountMapper bankAccountMapper) {
        this.bankAccountRepository = bankAccountRepository;
        this.bankAccountMapper = bankAccountMapper;
    }

    public List<BankAccount> getAllBankAccounts(){
        return StreamSupport.stream(bankAccountRepository.findAll().spliterator(), false)
                .map(bankAccountMapper::toModel)
                .collect(Collectors.toList());
    }

    public BankAccount getBankAccount(Long id) throws NotFoundException {
        Optional<BankAccount> bankAccount = bankAccountRepository.findById(id).map(bankAccountMapper::toModel);
        return bankAccount.orElseThrow(() -> new NotFoundException(String.format("Hospital not found with id:%S", id)));
    }
}
