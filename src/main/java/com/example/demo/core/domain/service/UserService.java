package com.example.demo.core.domain.service;

import com.example.demo.client.someapi.SomeClient;
import com.example.demo.core.application.dto.ChangeRoleRequestDto;
import com.example.demo.core.application.dto.PageDto;
import com.example.demo.core.application.dto.UserDto;
import com.example.demo.core.database.entity.Role;
import com.example.demo.core.database.entity.UserEntity;
import com.example.demo.core.database.repositories.UserRepository;
import com.example.demo.core.domain.model.User;
import com.example.demo.core.mapper.UserMapper;
import javassist.NotFoundException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    private final SomeClient someClient;

    public UserService(UserRepository userRepository, UserMapper userMapper, SomeClient someClient) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.someClient = someClient;
    }

    public PageDto<UserDto> getAllUsers(Integer startIndex, Integer size) {
        PageDto<UserDto> result = new PageDto<>();
        if (size > 0) {
            PageRequest pageRequest = PageRequest.of(startIndex, size);
            Page<UserDto> page = getPage(pageRequest)
                    .map(userMapper::toDto);
            result.setContent(page.getContent());
            result.setPagesNumber(page.getTotalPages());
            result.setElementsNumber(page.getTotalElements());
        } else {
            List<User> list1 = StreamSupport.stream(userRepository.findAll().spliterator(), false)
                    .map(userMapper::toModel)
                    .collect(Collectors.toList());
            List<UserDto> list2 = list1.stream()
                    .map(userMapper::toDto)
                    .collect(Collectors.toList());
            result.setContent(list2);
        }
        return result;
    }

    public List<User> getAllUsersFromClient(){
       return someClient.getAllUsers();
    }

    public Page<User> getPage(PageRequest pageRequest) {
            return userRepository.findAll(pageRequest)
                    .map(userMapper::toModel);
    }

    public User getUser(Long id) throws NotFoundException {
        Optional<User> user = userRepository.findById(id).map(userMapper::toModel);
        return user.orElseThrow(() -> new NotFoundException(String.format("User not found with id:%S", id)));
    }

    public void addUser(UserDto userDto) throws IllegalArgumentException {
           userRepository.save(userMapper.toEntity(userDto));
    }

    public void deleteById(Long id)  {
        userRepository.deleteById(id);
    }

    public void deleteAllUsers() {
        userRepository.deleteAll();
    }

    public void replaceUser(UserDto userDto, Long id) throws NotFoundException {
        Optional<UserEntity> newUser = userRepository.findById(id);
        newUser.map(userEntity -> {
            userEntity.setGender(userDto.getGender());
            userEntity.setFirstName(userDto.getFirstName());
            userEntity.setLastName(userDto.getLastName());
            userEntity.setEmail(userDto.getEmail());
            userEntity.setAge(userDto.getAge());
            return userRepository.save(userEntity);
        }).orElseThrow(() -> new NotFoundException(String.format("User not found with id: %S", id)));
    }

    public List<User> groupUsersByAge() {
        return StreamSupport.stream(userRepository.groupByAge().spliterator(), false)
                .map(userMapper::toModel)
                .collect(Collectors.toList());
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User with this email not found in database"));
        return userMapper.toModel(userEntity);
    }

     public void changeRole(Long id, Role role) {
        userRepository.changeRoleOfUser(role.getName(), id);
     }
}

