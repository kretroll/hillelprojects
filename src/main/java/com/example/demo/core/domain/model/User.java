package com.example.demo.core.domain.model;

import com.example.demo.core.database.entity.Role;
import lombok.Getter;
import com.example.demo.core.database.entity.BankAccountEntity;
import com.example.demo.core.database.entity.Gender;
import com.example.demo.core.database.entity.HospitalEntity;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;


@Getter
@Setter
public class User implements UserDetails {
    private Gender gender;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
    private Integer age;
    private String password;
    private HospitalEntity hospital;
    private List<BankAccountEntity> bankAccounts;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList((GrantedAuthority) () -> role.getName());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
